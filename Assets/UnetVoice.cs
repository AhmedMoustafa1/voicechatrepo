﻿using Dissonance.Integrations.UNet_LLAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnetVoice : MonoBehaviour {
    public UNetCommsNetwork dissonanceNetwork;
    public ushort port;
   


	// Use this for initialization
	void Start () {
        dissonanceNetwork.Port = port;
        dissonanceNetwork.InitializeAsClient("kandooz.studio");
    }

    // Update is called once per frame
    void Update () {
        
    }
}
